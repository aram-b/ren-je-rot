import React from 'react';
import {
	GlobalStyle,
	ThemeProvider,
} from '@datapunt/asc-ui';
import Scatter from './components/Scatter';

// https://bl.ocks.org/Andrew-Reid/976e51f97cffa1835bc03ca80c428ee7

function App() {
	return (
		<ThemeProvider>
			<GlobalStyle />
				<Scatter />
		</ThemeProvider>
	);
}

export default App;