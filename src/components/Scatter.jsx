import React, { useState, useEffect, useRef } from 'react';
import * as d3 from 'd3';
import { Heading } from '@datapunt/asc-ui';
import Grid from 'd3-v4-grid';

import styled from '@datapunt/asc-core';

// http://bl.ocks.org/herrstucki/5684816
// https://github.com/felixlaumon/d3.layout.grid

const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

const StyledSvgContainer = styled.div`
	width: 530px;
	height: 530px;
	background-color: papayawhip;
`

function update(data, svg, margin) {
	const width = svg.node().getBoundingClientRect().width;
	const height = svg.node().getBoundingClientRect().height;

	const grid = Grid()
		.bands(true)
		.size([width, height]);

	grid.data(data) // input data in grid layout
	grid.layout() // calculate new coordinates

	// use data with coordinates to create nodes
	const nodes = svg.selectAll('rect').data(grid.nodes(), function(d) { return d.id; });
	
	// create actual nodes
	nodes.enter()
		.append('rect')
		.style('stroke', 'black')
		.attr("height", 1e-9)
		.attr("width", 1e-9)
		.attr("transform", "translate(" + width/2 + "," + height/2 +  ")");
	
	// transform coordinates to sort nodes
	nodes
		.transition().duration(1000)
		.attr("transform", function(d) { return "translate(" + parseInt(d.x + margin) + "," + parseInt(d.y + margin) + ")"; })
}

const Scatter = () => {

	const svgRef = useRef();
	const [data, setData] = useState();

	const svg = d3.select(svgRef.current)
		.style('width', '100%')
		.style('height', '100%');

	// todo: dit moet onresize veranderen
	let margin = 15;

	// get data
	useEffect(() => {
		fetch('./data.json')
			.then(response => response.json())
			.then(json => setData(json))
	}, [])

	// onload
	if (svg && data) {
		// create nodes
		update(data, svg, margin);

		// set size and position of nodes
		svg.selectAll('rect').transition().duration(1000)
			.attr("height", 20)
			.attr("width", 20)
			.attr("transform", function(d) { return "translate(" + parseInt(d.x + margin) + "," + parseInt(d.y + margin) + ")"; })
	}

	const handleClick = (evt) => {
		if (svg && data) {
			if (evt.target.value === 'gesl') {
				const sortedData = data.sort((a,b) => (a.gesl > b.gesl) ? 1 : ((b.gesl > a.gesl) ? -1 : 0));
				update(sortedData, svg, margin);
			} else if (evt.target.value === 'opl') {
				const sortedData = data.sort((a,b) => (a.opl > b.opl) ? 1 : ((b.opl > a.opl) ? -1 : 0));
				update(sortedData, svg, margin);
			} else if (evt.target.value === 'toongesl') {
				svg.selectAll('rect')
					.style('fill', d => d.gesl === 'man' ? 'red' : 'black')
			} else if (evt.target.value === 'sortx') {
				// check data op unieke categorie
				// loop door categorieen en deel grid door aantal categorieen (in eerste instantie)
				// bereken x en y voor eerste grid, dan voor 2e grid
			}
		}
	}

	return (
		<Container>
			<Heading>Ren je rot</Heading>
			Sort by
			<button onClick={handleClick} value='toongesl'>Toon geslacht</button>
			<button onClick={handleClick} value='gesl'>Sorteer op geslacht</button>
			<button onClick={handleClick} value='opl'>Sorteer op opleiding</button>
			<StyledSvgContainer>
				<svg ref={svgRef} />
			</StyledSvgContainer>
		</Container>
	)
}

export default Scatter;